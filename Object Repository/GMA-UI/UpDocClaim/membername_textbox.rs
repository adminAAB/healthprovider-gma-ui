<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>membername_textbox</name>
   <tag></tag>
   <elementGuidId>b5ab1da9-839b-4298-b59c-cbd7b712ee89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;txtNamaPeserta&quot;]/div[2]/div[1]/div/input[count(. | //*[@ref_element = 'Object Repository/frame_set']) = count(//*[@ref_element = 'Object Repository/frame_set'])]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;txtNamaPeserta&quot;]/div[2]/div[1]/div/input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/frame_set</value>
   </webElementProperties>
</WebElementEntity>
