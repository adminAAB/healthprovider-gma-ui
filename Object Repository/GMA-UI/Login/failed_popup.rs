<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>failed_popup</name>
   <tag></tag>
   <elementGuidId>7e8f7f50-fd73-4e65-9409-f2b191a97205</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;D_a2is_Modal_Ttl&quot;][count(. | //*[@ref_element = 'Object Repository/frame_set']) = count(//*[@ref_element = 'Object Repository/frame_set'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;D_a2is_Modal_Ttl&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/frame_set</value>
   </webElementProperties>
</WebElementEntity>
