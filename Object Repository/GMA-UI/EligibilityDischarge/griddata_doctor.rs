<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>griddata_doctor</name>
   <tag></tag>
   <elementGuidId>42099002-23e4-491c-a67c-e5c16708035a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;dtLookUpData&quot;]/div[2]/div/table[count(. | //*[@ref_element = 'Object Repository/frame_set']) = count(//*[@ref_element = 'Object Repository/frame_set'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;dtLookUpData&quot;]/div[2]/div/table</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/frame_set</value>
   </webElementProperties>
</WebElementEntity>
