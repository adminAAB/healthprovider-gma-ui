<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>enddate_datepicker</name>
   <tag></tag>
   <elementGuidId>71efdd95-673e-4841-9522-846e2d3bc8ab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[4]/div/div[1]/table/tbody/tr[*]/td[@class='day'][count(. | //*[@ref_element = 'Object Repository/frame_set']) = count(//*[@ref_element = 'Object Repository/frame_set'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[4]/div/div[1]/table/tbody/tr[*]/td[@class='day']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/frame_set</value>
   </webElementProperties>
</WebElementEntity>
