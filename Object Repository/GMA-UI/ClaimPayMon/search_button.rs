<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>search_button</name>
   <tag></tag>
   <elementGuidId>7b953f59-8cff-46c8-b4c6-0b99ca7609f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;btnMainSearching&quot;]/div/div/button[2][count(. | //*[@ref_element = 'Object Repository/frame_set']) = count(//*[@ref_element = 'Object Repository/frame_set'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;btnMainSearching&quot;]/div/div/button[2]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/frame_set</value>
   </webElementProperties>
</WebElementEntity>
