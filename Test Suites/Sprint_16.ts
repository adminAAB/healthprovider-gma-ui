<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sprint_16</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>447b4b5d-dd86-40b8-a087-efd9b55fa4f0</testSuiteGuid>
   <testCaseLink>
      <guid>a0c77ae0-d705-410c-92e2-e91d60b3c72e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00008 - Get Outstanding - by Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46ccf84e-fe5d-47d9-a5b3-8dcd81eb3738</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00009 - Get Outstanding - by Date - Blank StartDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>876b97ee-c0d9-4955-ab80-b8a8a5bdb1c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00011 - Get Outstanding - by MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b96263cf-7aa3-4b43-b281-6a1e4223a755</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00013 - Get Outstanding - by MemberName - Invalid MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8aee0c26-2366-4014-845e-282e1c46927f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint 15/S15_2 - Get Outstanding - Date X MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb1ba0e4-a722-4825-b767-c4332dabd04d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00014 - Get Outstanding - Blank All Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>118ece03-ad10-43aa-9a07-7e3701d289bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00015 - Get Pending - by Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9bd61fbc-c8ec-47a8-bc35-3742d2c63aac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00017 - Get Pending - by Date - Blank StartDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fae8fe76-6883-4818-a062-1e2ee70940cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00018 - Get Pending - by MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3116d17e-6376-4ea3-b0e0-17c1069a40d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00020 - Get Pending - by MemberName - Invalid MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>98afc9fd-8c24-499f-a309-da9f35fdfdef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint 15/S15_3 - Get Pending - Date X MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51e30ff5-1931-449b-b370-a0049c2e00a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00021 - Get Pending - Blank All Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a035d51-de5e-4c0f-8c51-8f3dc9c2da7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00022 - Get Payment Monitoring - by InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e4636a2-0bf2-46ca-85fe-4a3e3b8e7be1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00023 - Get Payment Monitoring - by InvoiceNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>742c3841-a35c-4ea8-a072-5f0658d94f00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00025 - Get Payment Monitoring - by PasienName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>596354d8-fe35-4d42-a27b-219b2e827b82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00026 - Get Payment Monitoring - by PasienName - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bfe3fed2-8f2a-4038-a9e2-fa3b376c6a33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00029 - Get Payment Monitoring - by Date - Blank StartDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>283313cb-d639-48e7-97f9-82f0f1ae77ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00028 - Get Payment Monitoring - by Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3825edf7-e641-4eb6-9f79-3c5b132fb3cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00031 - Get Payment Monitoring - by ClaimNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>260d7485-467d-4052-a106-fcf3d20fd962</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00032 - Get Payment Monitoring - by ClaimNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad0c1b65-9128-426d-883c-1ba3a05782b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00034 - Get Payment Monitoring - by Company X InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>441f4620-dd71-4e9d-b2ff-4e80c6bd3c78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00035 - Get Payment Monitoring - by Company X InvoiceNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac3b3868-1083-4944-8396-7336fa0df022</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00046 - Get Payment Monitoring - by ClaimStatus X NamaPasien</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa59aa12-3876-4a62-8dbc-267a95dd11c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00053 - Get Payment Monitoring - by ClaimStatus X Date - Blank Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>288007ef-93c4-4be1-a817-2e425fe3dca8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00054 - Get Payment Monitoring - Blank All Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a16cf71d-77b6-43f2-9073-c4657c6f6913</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_001 - Download Excel Data - Search By Invoice No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>082444d1-73dd-4c9c-8274-584c6ed589b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_002 - Download Excel Data - Search By Pasien Name</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f891471-2176-4d5d-9a5f-8b35c817171e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_003 - Download Excel Data - Search By Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96031cb0-9deb-45de-a3ae-f5a9a13a9b4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_004 - Download Excel Data - Search By ClaimStatus X InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4d41cd1-8ed6-4cb6-937d-2541bae6bf6e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_005 - Download Excel Data - Search By Company X InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>141882ca-c772-40ca-bb08-ebbaddbe96f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_006 - Download Excel Data - Search By ClaimNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68c6a4ba-e5b5-4a17-b992-0ef909f907e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_007 - Download Excel Data - Search By InvoiceNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67138f07-03cd-4d72-8abd-d86e0dd64e2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_008 - Download Excel Data - Search By PasienName - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee008352-0b90-448f-af9a-90012bbaa554</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_009 - Download Excel Data - Search By Date - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>277d0438-eec0-4987-bda0-8ea5240234db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_010 - Download Excel Data - Search By ClaimNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c9dc74b-c70f-4a6d-9e72-6edd0f23f958</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_011 - Download Excel Data - Search By Company X InvoiceNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a05c5db7-6368-47a5-a70e-2b25f8d92f19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_012 - Download Excel Data - Search By ClaimStatus X Date - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe22e444-b866-44d6-a8da-94f392ee2c73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_013 - Upload Document Outstanding - JPG File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93edfb9c-8031-466d-b47c-aba1ff3ab1ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_014 - Upload Document Outstanding - PNG File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>adb4a634-fb87-4a7b-aabe-ecb97b3f9d48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_015 - Upload Document Outstanding - PDF File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2bbfeba-dcc1-48a7-9d7b-57e91485369e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_016 - Upload Document Outstanding - BMP File - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>489f974f-75c5-4290-a86a-d2a2b10d2a53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_017 - Upload Document Outstanding - Excel File - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae3ea646-4699-45e7-af3f-2f5e344dc270</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_018 - Upload Document Outstanding - SQL File - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fee403d4-d4fd-44cc-9a7f-249771fb4ca2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_019 - Upload Document Outstanding - Max Size - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d02e3a5-f7cf-408e-b324-253d4cef9dd4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_020 - Upload Document Outstanding - Blank Data - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7fcfafcd-50b0-4c43-aff5-290c361efe2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_021 - Upload Document Pending - JPG File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff16674b-c926-4736-b044-a2e9d0c8e065</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_022 - Upload Document Pending - PNG File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5f5063f-b94d-49e7-b079-1490ffd2ff71</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_023 - Upload Document Pending - PDF File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>867683b2-36d2-44d9-bdbe-331c5cdecb5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_024 - Upload Document Pending - BMP File - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba540794-3e9f-4acf-bddc-629a1b4ac775</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_025 - Upload Document Pending - Excel File - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de2bdf62-1ecb-4c4e-bf9f-e3bc8b149e4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_026 - Upload Document Pending - SQL File - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe0ce8d7-6f34-4040-af27-99bc6149d7e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_027 - Upload Document Pending - Max File - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae2f4008-3d43-4bc9-8f49-490c44a258f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_028 - Upload Document Pending - Blank Data - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d47adb0-2ad4-4b86-8586-32821c502217</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_029 - Submit Document Outstanding</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3aa12183-b818-4704-bd06-7475d2f36bd5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>da13712b-9332-40b5-a630-0874b971dd89</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_030 - Submit Document Outstanding - Invalid</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f5ffe150-9488-4752-95a9-77a97ee1fe06</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>58ea6324-a3d3-4d1b-8bcb-adfdce2eea6d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_031 - Submit Document Pending</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a5eea562-433f-401e-aa10-92950fec52e9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6fad5e7a-418d-4c5a-9616-e91dbdb1500b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_032 - Submit Document Pending - Invalid</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>70bc0080-f993-4b98-a885-09e75ac0e966</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>bd5488e7-091f-4d80-9188-10ca4eb83081</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_033 - Kesesuaian GMA X CoreHealth - OutstandingData</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>46073970-7a16-44e1-84cf-b1596ebfaac0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3faab29c-43dc-4cf4-997f-41019201531c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_034 - Kesesuaian GMA X CoreHealth - PendingData</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c193b46a-24c3-456f-9eee-9e2174638449</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>aaf2527f-805d-4456-8334-dce8c6ca8dda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00022 - Get Payment Monitoring - by InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d88b5d6d-35f9-416a-be5b-c41a6ce278f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00023 - Get Payment Monitoring - by InvoiceNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07a987bf-bedc-48fa-8740-ed3ad9bb97bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_036 - Get Payment Monitoring - by InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85e49d1f-7bbd-48d5-816a-a1857533d6ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_037 - Get Payment Monitoring - by PasienName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f0c01eb-76c6-41c0-b313-c8311621a768</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_038 - Get Payment Monitoring - by Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0fc9001-11f9-4cd2-a41c-cef95a47428c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_039 - Get Payment Monitoring - by ClaimNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7e1b196-7ec0-4076-a737-1b35e6d97f83</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_035 - Kesesuaian Sum Discount</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82e31384-48ac-4aa4-8838-bcdc09c74ae2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_040 - Get Payment Monitoring - by Company X InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>033bd87f-187a-43cb-ac07-dbb8efd418eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_041 - Get Payment Monitoring - by ClaimStatus X InvoiceNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
