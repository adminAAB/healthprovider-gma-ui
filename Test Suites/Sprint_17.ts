<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sprint_17</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>395acf0f-4b9b-4cfe-b101-b9368822a979</testSuiteGuid>
   <testCaseLink>
      <guid>37628b7c-b572-4920-80b8-113490bc2bf5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_001 - Open Void Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c9ad039-2e69-4aaa-887c-98662b8116b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_002 - Get Member Void - Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7075c025-877e-424a-a7c8-c0407c0047a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_003 - Get Member Void - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed81640e-4b0b-4b3e-8628-ee3eec11914b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_004 - Submit Void - Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e3b8bc8-8ef8-4fd5-a0dd-10aaab957d70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_005 - Submit Void - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>27788c26-597d-44a3-896b-927613067655</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_006 - Submit Void - Blank</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49409741-442e-44cc-bc25-390de3bc6581</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_007 - Submit Void - More 500 Char Reason</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dffb0160-620a-4026-809a-bbf2fae4c49b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_008 - Open Discharge Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a83e84a-a946-4d56-833c-e7980cbf8153</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_009 - Get Member Discharge - Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2eecee93-3449-4227-bd15-973f077e87d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_010 - Get Member Discharge - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>115b2293-25b2-4ba2-bb65-02dd57c4a00e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_012 - Get Doctor - Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8a45c64-e53d-4f0d-a04a-fa69192b56dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_013 - Get Doctor - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94cee365-69bf-4341-82ae-009ce269a94c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_011 - Get Member Discharge - Blank</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fff0e197-8e9e-4a3d-ae89-5b97ffb83eb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_014 - Get Doctor - Input Freetext</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75340431-1494-405e-b6e0-df5447cd3053</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_015 - Submit Discharge - Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>819571b0-8aa2-439f-87cc-76ebb7f338a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_016 - Submit Discharge - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>839e74b9-d6e4-4e7d-8daa-7b159d7fbc14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_036 - Get Payment Monitoring - by InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>965a8d55-4f6b-4f68-817e-c331f2ebfe7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_037 - Get Payment Monitoring - by PasienName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4add91c-908a-4399-9974-f2e73d472607</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_038 - Get Payment Monitoring - by Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a05d0133-339a-4355-aa83-272270b91104</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_039 - Get Payment Monitoring - by ClaimNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>814910e8-52f1-4a68-9e1a-2e8943eb25f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_040 - Get Payment Monitoring - by Company X InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3fe597f-42fe-4193-af87-26aae637efc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_041 - Get Payment Monitoring - by ClaimStatus X InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>928dbfe0-39c4-42ce-b9b6-15ad643b4f73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_001 - Download Excel Data - Search By Invoice No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>380da60f-6c07-40c9-8f27-bd6be714dbce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_002 - Download Excel Data - Search By Pasien Name</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4eb0e8d0-a6a2-47f7-a1b3-6e788b27ab46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_003 - Download Excel Data - Search By Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>067bf485-166a-4b73-bf8f-d5f44557a6b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_004 - Download Excel Data - Search By ClaimStatus X InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d98d793f-dee6-4172-ae25-dd9c14ef9aeb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_005 - Download Excel Data - Search By Company X InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d96c9e0-4dd5-487e-b6d5-d392eef674af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_006 - Download Excel Data - Search By ClaimNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1132b39b-0976-4115-ae80-58b16b2b38d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_007 - Download Excel Data - Search By InvoiceNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34727206-4891-49ae-8f06-1cd4f24cc6e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_008 - Download Excel Data - Search By PasienName - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a12dfb0-6033-43a1-a999-4b1cfdcc0878</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_009 - Download Excel Data - Search By Date - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50f7a27f-e080-435c-8081-4bf1d9f39bde</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_010 - Download Excel Data - Search By ClaimNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbf25f83-e9da-49e4-8a4f-006b0123fa07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_011 - Download Excel Data - Search By Company X InvoiceNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2fbb5ea2-0bb8-4ee6-98bb-5af3b9116b1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_012 - Download Excel Data - Search By ClaimStatus X Date - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e6862eb-89dc-49e7-89d7-89f2636025bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_035 - Kesesuaian Sum Discount</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2f695d9-f8e1-4a0d-8597-80aeb8039535</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00029 - Get Payment Monitoring - by Date - Blank StartDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2dfe012-2abd-4d86-bc5d-3986eced6403</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00030 - Get Payment Monitoring - by Date - Blank EndDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55148ce5-0cd2-4a26-9b56-a4c1a7b4be24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00028 - Get Payment Monitoring - by Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b78ccb8b-a173-4fca-83ec-8d49a920f8ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00022 - Get Payment Monitoring - by InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81b80f3b-7949-44ce-a59b-ddc61f311e10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00023 - Get Payment Monitoring - by InvoiceNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e7c7c95-26dd-4ddb-9729-da07e4563bad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_017 - Get Payment Monitoring OP - by InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce3c3ecb-4b55-414d-8763-fec0840e34a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_17/S17_018 - Get Payment Monitoring OP - by Date</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
