<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Security_Test</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>34a6a869-8718-4b4c-8cea-51119ba10f13</testSuiteGuid>
   <testCaseLink>
      <guid>2280a0d4-267b-40de-9541-c031e0749553</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00001 - Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0d9642a-ffb4-4bb7-9c5f-be13a7c1e841</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00002 - Login - Username Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49305d9f-764c-4b53-8ec9-c94660c9ffea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00003 - Login - Username Blank Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e1ed046-d20f-4a8c-955f-e4243180ad93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00004 - Login - Password Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e5ccdf4-606e-4b1b-9d19-6b4b95b366b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00005 - Login - Password Blank Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38995034-f66e-4010-9351-ae530e2fe12a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00006 - Login - Username n Password Blank Data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
