<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sprint_18</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5425cd6a-7841-4769-8ec1-40f9b7c7d532</testSuiteGuid>
   <testCaseLink>
      <guid>e23fe14b-5039-4021-8ee5-ac9c82c94ba9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00007 - Upload Document Claim - Tampilan Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6f5f53e-3c56-4221-9642-e9370840b6f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00011 - Get Outstanding - by MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b41dc95f-74e9-442a-a0ac-b33febb22176</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_033 - Kesesuaian GMA X CoreHealth - OutstandingData</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>46073970-7a16-44e1-84cf-b1596ebfaac0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>68fbc075-3021-44a6-98f0-3235a332fb0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00008 - Get Outstanding - by Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b1a60a9-105c-4656-b5ac-0288be84a9ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00010 - Get Outstanding - by Date - Blank EndDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e6add5c-40c0-4424-9527-877f3422f4a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00011 - Get Outstanding - by MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8906cdf-3fbb-492e-abd9-09847622d056</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00013 - Get Outstanding - by MemberName - Invalid MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f3a2f55-39ba-4a3b-affe-9b124d1c02de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00014 - Get Outstanding - Blank All Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04927b68-a187-45b7-929e-ba397c64a043</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint 15/S15_2 - Get Outstanding - Date X MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d11991a-20ae-4fa8-bdb5-8002a92b1afb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_013 - Upload Document Outstanding - JPG File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3058edc-f398-41a3-ba82-b6ab8aed87cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_014 - Upload Document Outstanding - PNG File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8cb7cb34-f229-443c-aad0-eff382d52f77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_015 - Upload Document Outstanding - PDF File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>554cbff7-82bb-4bed-847f-b66e2eca8155</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_016 - Upload Document Outstanding - BMP File - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>509b755d-08bd-4805-826f-3816d2f314fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_017 - Upload Document Outstanding - Excel File - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d44ab3e7-1771-4e18-96aa-e8598bf788d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_018 - Upload Document Outstanding - SQL File - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>702bd197-791e-452c-bb0f-67f2b5817f7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_019 - Upload Document Outstanding - Max Size - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb597e7b-49ca-4e31-955b-2037a09812d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_020 - Upload Document Outstanding - Blank Data - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb943a1a-2bb4-446a-b982-8fcfc4a14e10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_029 - Submit Document Outstanding</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3aa12183-b818-4704-bd06-7475d2f36bd5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>62c4bf6e-3037-4cc2-bda6-61bcd32213b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint_16/S16_030 - Submit Document Outstanding - Invalid</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f5ffe150-9488-4752-95a9-77a97ee1fe06</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
