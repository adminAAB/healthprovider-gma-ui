<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sprint_15</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9ba1d8ac-c01b-4ac4-83a2-a46974868b10</testSuiteGuid>
   <testCaseLink>
      <guid>5953bfe0-7ebf-46d4-ac4d-dc67f6b7bce6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00007 - Upload Document Claim - Tampilan Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b534ff33-4778-43b1-a408-f2d1ea08c16a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00008 - Get Outstanding - by Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>201fb5a8-ffaf-4b35-aaee-f99595151622</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00009 - Get Outstanding - by Date - Blank StartDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39c6163a-24f2-42a4-a8cf-14bf4b7564ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00010 - Get Outstanding - by Date - Blank EndDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>758c1382-ff46-4ba8-a16f-77c30e6f7905</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00011 - Get Outstanding - by MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b12cceb-247e-412e-bdad-9d446e0a4670</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00012 - Get Outstanding - by MemberName - Blank MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a48bff5d-43cc-4ffa-8a97-e9572e540bed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00013 - Get Outstanding - by MemberName - Invalid MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70b34980-0750-4793-a42d-64449695e4bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00014 - Get Outstanding - Blank All Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af43f617-ce95-47a4-83b6-d518a511ffff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00015 - Get Pending - by Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8d7cdb3-8fb8-4a2a-b0dd-bba3b91081da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00016 - Get Pending - by Date - Blank EndDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25a4eb36-6435-4ea6-898d-dd9238969db8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00017 - Get Pending - by Date - Blank StartDate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2f1fb08-fd9b-4945-b882-7de82ea5587b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00018 - Get Pending - by MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7325859c-3e35-4872-8731-f114decc27f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00019 - Get Pending - by MemberName - Blank MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81f465eb-0cc1-4a5b-9c62-a0160966b35c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00020 - Get Pending - by MemberName - Invalid MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47e8ee7a-8a02-4110-b68d-bbffd453c0df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00021 - Get Pending - Blank All Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a18d4217-189c-4386-b5a1-ab0cc8d3736d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00022 - Get Payment Monitoring - by InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80b7bf9f-f036-42fa-b793-0f4389f8b724</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00023 - Get Payment Monitoring - by InvoiceNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3d72dd2-e764-4292-9971-626ab5a500f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00025 - Get Payment Monitoring - by PasienName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd36739f-fe3c-494a-b2f2-0f45112396a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00026 - Get Payment Monitoring - by PasienName - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd2efbb9-0a40-495f-89c3-1d286d42b95e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00028 - Get Payment Monitoring - by Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccdc9345-e48a-4fb8-bd4a-33d95b622694</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00031 - Get Payment Monitoring - by ClaimNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>508e0eda-beee-46aa-abe9-d75102e5b2f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00032 - Get Payment Monitoring - by ClaimNo - Invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69a06ad4-73e5-4c68-b908-75c06bf50f4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00034 - Get Payment Monitoring - by Company X InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f3d9168-a8c4-40f5-94e6-d39883a3d19a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00037 - Get Payment Monitoring - by Company X PasienName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>802590a0-7985-4002-95a0-ee66710bc936</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00040 - Get Payment Monitoring - by Company X ClaimNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8e30d1d-db98-4b89-ae05-d5ffc85f4cc7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00043 - Get Payment Monitoring - by Company X Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c7acb6d-b7d7-4034-a577-72b16e45c438</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00046 - Get Payment Monitoring - by ClaimStatus X NamaPasien</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>645c9904-12a4-4085-8c77-703873963a1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00050 - Get Payment Monitoring - by ClaimStatus X ClaimNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14751a12-16f8-4256-aaaa-ab4b4529cffd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00048 - Get Payment Monitoring - by ClaimStatus X InvoiceNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a046745a-8a30-4ab7-9f71-fa222ed98761</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00052 - Get Payment Monitoring - by ClaimStatus X Date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c45e752-b152-4048-a4d2-cd962f2c0673</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_TEST/00054 - Get Payment Monitoring - Blank All Data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a101a90-4089-4628-abab-0dd614efc27e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint 15/S15_1 - Get Outstanding - Cek CutOff</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c117fc7e-a38e-42f0-904c-cc025f60ac78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint 15/S15_2 - Get Outstanding - Date X MemberName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7623b34d-6c0c-409e-ada4-6b834ac6882f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Scenario/_SCRUM/Sprint 15/S15_3 - Get Pending - Date X MemberName</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
