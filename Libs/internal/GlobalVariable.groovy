package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object maxDelay
     
    /**
     * <p></p>
     */
    public static Object MediumDelay
     
    /**
     * <p></p>
     */
    public static Object DefaultDelay
     
    /**
     * <p></p>
     */
    public static Object DownloadFolder
     
    /**
     * <p>Profile default : Diubah sesuai destinasi appium</p>
     */
    public static Object AppiumPath
     
    /**
     * <p></p>
     */
    public static Object AppiumPort
     
    /**
     * <p>Profile default : Forbidden</p>
     */
    public static Object CurrentLanguage
     
    /**
     * <p>Profile default : Forbidden</p>
     */
    public static Object CurrentDevice
     
    /**
     * <p></p>
     */
    public static Object ScreenshotPath
     
    /**
     * <p>Profile default : Debug, Release</p>
     */
    public static Object ScreenshotFolder
     
    /**
     * <p>Profile default : Forbidden</p>
     */
    public static Object ScreenshotPhone
     
    /**
     * <p>Profile default : Forbidden</p>
     */
    public static Object ScreenshotTS
     
    /**
     * <p>Profile default : Forbidden</p>
     */
    public static Object ScreenshotUrutan
     
    /**
     * <p></p>
     */
    public static Object ScreenshotSet
     
    /**
     * <p></p>
     */
    public static Object ScreenshotTime
     
    /**
     * <p></p>
     */
    public static Object intNumCap
     
    /**
     * <p></p>
     */
    public static Object intIdAddPanel_LK
     
    /**
     * <p></p>
     */
    public static Object dataPanel
     
    /**
     * <p></p>
     */
    public static Object intColoumn
     
    /**
     * <p></p>
     */
    public static Object SavedValue1
     
    /**
     * <p></p>
     */
    public static Object SavedValue2
     
    /**
     * <p></p>
     */
    public static Object StickerID
     
    /**
     * <p>Profile default : Tidak perlu diubah</p>
     */
    public static Object URLGen5
     
    /**
     * <p></p>
     */
    public static Object RegistrationNo
     
    /**
     * <p></p>
     */
    public static Object ChassisNo
     
    /**
     * <p></p>
     */
    public static Object EngineNo
     
    /**
     * <p></p>
     */
    public static Object ProspectName
     
    /**
     * <p></p>
     */
    public static Object GodigPrefix
     
    /**
     * <p></p>
     */
    public static Object GodigVANumber
     
    /**
     * <p></p>
     */
    public static Object GodigPrice
     
    /**
     * <p></p>
     */
    public static Object GodigOrderNo
     
    /**
     * <p>Profile default : Diubah sesuai lokasi file setelah di cloning</p>
     */
    public static Object uploadFileLoc
     
    /**
     * <p>Profile default : Diubah sesuai lokasi file setelah di cloning</p>
     */
    public static Object uploadFileName
     
    /**
     * <p>Profile default : Diubah sesuai lokasi file setelah di cloning</p>
     */
    public static Object uploadFileDestination
     
    /**
     * <p></p>
     */
    public static Object ShortenURL
     
    /**
     * <p></p>
     */
    public static Object CustPhone
     
    /**
     * <p></p>
     */
    public static Object CustVehicle
     
    /**
     * <p></p>
     */
    public static Object CustUsage
     
    /**
     * <p></p>
     */
    public static Object CustRegion
     
    /**
     * <p></p>
     */
    public static Object ProductType
     
    /**
     * <p></p>
     */
    public static Object ProductCode
     
    /**
     * <p></p>
     */
    public static Object Coverage
     
    /**
     * <p></p>
     */
    public static Object PremiTJH
     
    /**
     * <p></p>
     */
    public static Object PremiSRCC
     
    /**
     * <p></p>
     */
    public static Object PremiFlood
     
    /**
     * <p></p>
     */
    public static Object PremiEarthquake
     
    /**
     * <p></p>
     */
    public static Object PremiTerrorism
     
    /**
     * <p></p>
     */
    public static Object PremiDriver
     
    /**
     * <p></p>
     */
    public static Object PremiPassanger
     
    /**
     * <p></p>
     */
    public static Object PremiAccessories
     
    /**
     * <p>Profile default : Not Used Yet</p>
     */
    public static Object Name
     
    /**
     * <p></p>
     */
    public static Object type
     
    /**
     * <p></p>
     */
    public static Object CashDealer
     
    /**
     * <p></p>
     */
    public static Object URLGen5_GMA
     
    /**
     * <p></p>
     */
    public static Object Username
     
    /**
     * <p></p>
     */
    public static Object Password
     
    /**
     * <p></p>
     */
    public static Object Token
     
    /**
     * <p></p>
     */
    public static Object MemberName
     
    /**
     * <p></p>
     */
    public static Object GMA_QueyGetOutstandingData
     
    /**
     * <p></p>
     */
    public static Object GMA_QueryGetPendingData
     
    /**
     * <p></p>
     */
    public static Object SearchScenarioData
     
    /**
     * <p></p>
     */
    public static Object DataParam
     
    /**
     * <p></p>
     */
    public static Object GMA_QueryGetInvoiceData
     
    /**
     * <p></p>
     */
    public static Object GMA_QueryGetInvoiceData2
     
    /**
     * <p></p>
     */
    public static Object totalDocUpload
     
    /**
     * <p></p>
     */
    public static Object totalDocVerify
     
    /**
     * <p></p>
     */
    public static Object SumTotalPayment
     
    /**
     * <p></p>
     */
    public static Object SumTotalBilled
     
    /**
     * <p></p>
     */
    public static Object SumTotalDiscount
     
    /**
     * <p></p>
     */
    public static Object SumTotalPaymentVerify
     
    /**
     * <p></p>
     */
    public static Object SumTotalBilledVerify
     
    /**
     * <p></p>
     */
    public static Object SumTotalDiscountVerify
     
    /**
     * <p></p>
     */
    public static Object temp1
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            maxDelay = selectedVariables['maxDelay']
            MediumDelay = selectedVariables['MediumDelay']
            DefaultDelay = selectedVariables['DefaultDelay']
            DownloadFolder = selectedVariables['DownloadFolder']
            AppiumPath = selectedVariables['AppiumPath']
            AppiumPort = selectedVariables['AppiumPort']
            CurrentLanguage = selectedVariables['CurrentLanguage']
            CurrentDevice = selectedVariables['CurrentDevice']
            ScreenshotPath = selectedVariables['ScreenshotPath']
            ScreenshotFolder = selectedVariables['ScreenshotFolder']
            ScreenshotPhone = selectedVariables['ScreenshotPhone']
            ScreenshotTS = selectedVariables['ScreenshotTS']
            ScreenshotUrutan = selectedVariables['ScreenshotUrutan']
            ScreenshotSet = selectedVariables['ScreenshotSet']
            ScreenshotTime = selectedVariables['ScreenshotTime']
            intNumCap = selectedVariables['intNumCap']
            intIdAddPanel_LK = selectedVariables['intIdAddPanel_LK']
            dataPanel = selectedVariables['dataPanel']
            intColoumn = selectedVariables['intColoumn']
            SavedValue1 = selectedVariables['SavedValue1']
            SavedValue2 = selectedVariables['SavedValue2']
            StickerID = selectedVariables['StickerID']
            URLGen5 = selectedVariables['URLGen5']
            RegistrationNo = selectedVariables['RegistrationNo']
            ChassisNo = selectedVariables['ChassisNo']
            EngineNo = selectedVariables['EngineNo']
            ProspectName = selectedVariables['ProspectName']
            GodigPrefix = selectedVariables['GodigPrefix']
            GodigVANumber = selectedVariables['GodigVANumber']
            GodigPrice = selectedVariables['GodigPrice']
            GodigOrderNo = selectedVariables['GodigOrderNo']
            uploadFileLoc = selectedVariables['uploadFileLoc']
            uploadFileName = selectedVariables['uploadFileName']
            uploadFileDestination = selectedVariables['uploadFileDestination']
            ShortenURL = selectedVariables['ShortenURL']
            CustPhone = selectedVariables['CustPhone']
            CustVehicle = selectedVariables['CustVehicle']
            CustUsage = selectedVariables['CustUsage']
            CustRegion = selectedVariables['CustRegion']
            ProductType = selectedVariables['ProductType']
            ProductCode = selectedVariables['ProductCode']
            Coverage = selectedVariables['Coverage']
            PremiTJH = selectedVariables['PremiTJH']
            PremiSRCC = selectedVariables['PremiSRCC']
            PremiFlood = selectedVariables['PremiFlood']
            PremiEarthquake = selectedVariables['PremiEarthquake']
            PremiTerrorism = selectedVariables['PremiTerrorism']
            PremiDriver = selectedVariables['PremiDriver']
            PremiPassanger = selectedVariables['PremiPassanger']
            PremiAccessories = selectedVariables['PremiAccessories']
            Name = selectedVariables['Name']
            type = selectedVariables['type']
            CashDealer = selectedVariables['CashDealer']
            URLGen5_GMA = selectedVariables['URLGen5_GMA']
            Username = selectedVariables['Username']
            Password = selectedVariables['Password']
            Token = selectedVariables['Token']
            MemberName = selectedVariables['MemberName']
            GMA_QueyGetOutstandingData = selectedVariables['GMA_QueyGetOutstandingData']
            GMA_QueryGetPendingData = selectedVariables['GMA_QueryGetPendingData']
            SearchScenarioData = selectedVariables['SearchScenarioData']
            DataParam = selectedVariables['DataParam']
            GMA_QueryGetInvoiceData = selectedVariables['GMA_QueryGetInvoiceData']
            GMA_QueryGetInvoiceData2 = selectedVariables['GMA_QueryGetInvoiceData2']
            totalDocUpload = selectedVariables['totalDocUpload']
            totalDocVerify = selectedVariables['totalDocVerify']
            SumTotalPayment = selectedVariables['SumTotalPayment']
            SumTotalBilled = selectedVariables['SumTotalBilled']
            SumTotalDiscount = selectedVariables['SumTotalDiscount']
            SumTotalPaymentVerify = selectedVariables['SumTotalPaymentVerify']
            SumTotalBilledVerify = selectedVariables['SumTotalBilledVerify']
            SumTotalDiscountVerify = selectedVariables['SumTotalDiscountVerify']
            temp1 = selectedVariables['temp1']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
