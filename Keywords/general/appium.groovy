package general

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.remote.DesiredCapabilities as DesiredCapabilities
import com.kms.katalon.core.appium.driver.AppiumDriverManager as AppiumDriverManager
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.mobile.driver.MobileDriverType as MobileDriverType
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import io.appium.java_client.android.AndroidDriver as AndroidDriver

import com.kms.katalon.core.webui.driver.DriverFactory

public class appium {
	private static Runtime runtime = null;
	private static Process proc = null;

	@Keyword

	def startServer() {
		runtime = Runtime.getRuntime()

		Random rand = new Random()

		int port = rand.nextInt(10000) + 50000

		GlobalVariable.AppiumPort = port + ""

		proc = runtime.exec('cmd.exe /c start cmd.exe /k node '+ GlobalVariable.AppiumPath +' --address 127.0.0.1 --port ' + port)
	}

	@Keyword

	def startDevice(String port, String appPath, Boolean noReset) {
		Integer retry = 10

		String UDID = AppiumDriverManager.getDeviceId(DriverFactory.MOBILE_DRIVER_PROPERTY)

		String currentPlatform = AppiumDriverManager.getDeviceOSVersion(DriverFactory.MOBILE_DRIVER_PROPERTY)

		String name = AppiumDriverManager.getDeviceModel(DriverFactory.MOBILE_DRIVER_PROPERTY)

		GlobalVariable.CurrentDevice =  AppiumDriverManager.getDeviceName(DriverFactory.MOBILE_DRIVER_PROPERTY)

		GlobalVariable.ScreenshotPhone = name

		def defaultLanguage = new general.mobile().getDeviceInfo(currentPlatform, "ID")

		String localServer = 'http://127.0.0.1:' + port + '/wd/hub'

		DesiredCapabilities capabilities = new DesiredCapabilities()

		capabilities.setCapability('platformName', 'Android')

		capabilities.setCapability('platformVersion', currentPlatform)

		capabilities.setCapability('udid', UDID)

		capabilities.setCapability('deviceName', name)

		capabilities.setCapability('automationName', 'UiAutomator2')

		capabilities.setCapability('app', appPath)

		capabilities.setCapability('noReset', noReset)

		capabilities.setCapability('newCommandTimeout', 3600)

		capabilities.setCapability('autoGrantPermissions', true)

		capabilities.setCapability('language', defaultLanguage[0])

		capabilities.setCapability('locale', defaultLanguage[1])

		while(retry>0){
			try{
				AppiumDriverManager.createMobileDriver(MobileDriverType.ANDROID_DRIVER, capabilities, new URL(localServer))

				return
			}
			catch (Exception ex){
				retry--
				Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)
			}
		}
		AppiumDriverManager.createMobileDriver(MobileDriverType.ANDROID_DRIVER, capabilities, new URL(localServer))
	}

	@Keyword

	def startDevice(String port, String appPath, Boolean noReset, String appPackage, String appActivity) {
		Integer retry = 10

		String UDID = AppiumDriverManager.getDeviceId(DriverFactory.MOBILE_DRIVER_PROPERTY)

		String currentPlatform = AppiumDriverManager.getDeviceOSVersion(DriverFactory.MOBILE_DRIVER_PROPERTY)

		String name = AppiumDriverManager.getDeviceModel(DriverFactory.MOBILE_DRIVER_PROPERTY)

		GlobalVariable.CurrentDevice =  AppiumDriverManager.getDeviceName(DriverFactory.MOBILE_DRIVER_PROPERTY)

		GlobalVariable.ScreenshotPhone = name

		def defaultLanguage = new general.mobile().getDeviceInfo(currentPlatform, "ID")

		String localServer = 'http://127.0.0.1:' + port + '/wd/hub'

		DesiredCapabilities capabilities = new DesiredCapabilities()

		capabilities.setCapability('platformName', 'Android')

		capabilities.setCapability('platformVersion', currentPlatform)

		capabilities.setCapability('udid', UDID)

		capabilities.setCapability('deviceName', name)

		capabilities.setCapability('automationName', 'UiAutomator2')

		capabilities.setCapability('app', appPath)

		capabilities.setCapability('appPackage', appPackage)

		capabilities.setCapability('appActivity', appActivity)

		capabilities.setCapability('noReset', noReset)

		capabilities.setCapability('newCommandTimeout', 3600)

		capabilities.setCapability('autoGrantPermissions', true)

		capabilities.setCapability('language', defaultLanguage[0])

		capabilities.setCapability('locale', defaultLanguage[1])

		while(retry>0){
			try{
				AppiumDriverManager.createMobileDriver(MobileDriverType.ANDROID_DRIVER, capabilities, new URL(localServer))

				return
			}
			catch (Exception ex){
				retry--
				Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)
			}
		}
		AppiumDriverManager.createMobileDriver(MobileDriverType.ANDROID_DRIVER, capabilities, new URL(localServer))
	}

	@Keyword

	def startDevice(String port, String appPath, Boolean noReset, String appPackage, String appActivity, String language) {
		Integer retry = 10

		String UDID = AppiumDriverManager.getDeviceId(DriverFactory.MOBILE_DRIVER_PROPERTY)

		String currentPlatform = AppiumDriverManager.getDeviceOSVersion(DriverFactory.MOBILE_DRIVER_PROPERTY)

		String name = AppiumDriverManager.getDeviceModel(DriverFactory.MOBILE_DRIVER_PROPERTY)

		GlobalVariable.CurrentDevice =  AppiumDriverManager.getDeviceName(DriverFactory.MOBILE_DRIVER_PROPERTY)

		GlobalVariable.ScreenshotPhone = name

		String localServer = 'http://127.0.0.1:' + port + '/wd/hub'

		DesiredCapabilities capabilities = new DesiredCapabilities()

		capabilities.setCapability('platformName', 'Android')

		capabilities.setCapability('platformVersion', currentPlatform)

		capabilities.setCapability('udid', UDID)

		capabilities.setCapability('deviceName', name)

		capabilities.setCapability('automationName', 'UiAutomator2')

		capabilities.setCapability('app', appPath)

		capabilities.setCapability('appPackage', appPackage)

		capabilities.setCapability('appActivity', appActivity)

		capabilities.setCapability('noReset', noReset)

		capabilities.setCapability('newCommandTimeout', 3600)

		capabilities.setCapability('autoGrantPermissions', true)

		capabilities.setCapability('language', language)

		capabilities.setCapability('locale', language)

		while(retry>0){
			try{
				AppiumDriverManager.createMobileDriver(MobileDriverType.ANDROID_DRIVER, capabilities, new URL(localServer))

				return
			}
			catch (Exception ex){
				retry--
				Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)
			}
		}
		AppiumDriverManager.createMobileDriver(MobileDriverType.ANDROID_DRIVER, capabilities, new URL(localServer))
	}

	@Keyword

	def stopServer() {

		//AppiumDriverManager.closeDriver()

		//Mobile.closeApplication()

		proc.destroy()

		proc.destroyForcibly()

		int a = proc.exitValue()

		Boolean b = proc.isAlive()
	}
}
