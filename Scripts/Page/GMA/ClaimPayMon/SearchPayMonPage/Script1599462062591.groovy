import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI


WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/ClaimPayMon/ClaimPayMonPage'),
	[ ('') : ''],
	FailureHandling.STOP_ON_FAILURE)

if (param == "invoiceno"){
	if (param2 == ""){
		
	} else {
		WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/invoiceno_textbox"), param2, FailureHandling.STOP_ON_FAILURE)
	}
} else if (param == "pasienname"){
	if (param2 == ""){
		
	} else {
		WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/pasienname_textbox"), param2, FailureHandling.STOP_ON_FAILURE)
	}
} else if (param == "date"){
	if (param2 == ""){
			
	} else {
		WebUI.click(findTestObject("GMA-UI/ClaimPayMon/startdate_icon"), FailureHandling.STOP_ON_FAILURE)
		GEN5.DatePicker(param2, findTestObject("GMA-UI/ClaimPayMon/startdate_datepicker"))
	}
	
	if (param3 == ""){
	
	// Fungsi GEN5.DatePicker tidak bisa, diganti fungsi DateNow dengan Clik Icon EndDate
	} else {
		WebUI.click(findTestObject("GMA-UI/ClaimPayMon/enddate_icon"), FailureHandling.STOP_ON_FAILURE)
		//GEN5.DatePicker(param3, findTestObject("GMA-UI/UpDocClaim/enddate_datepicker"))
		//GEN5.Write(findTestObject("GMA-UI/UpDocClaim/enddate_textbox"), enddate)
	}
} else if (param == "claimno"){
	if (param2 == ""){
		
	} else {
		WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/claimno_textbox"), param2, FailureHandling.STOP_ON_FAILURE)
	}
} else if (param == "company"){
	if (param1 == "invoiceno"){
		if (param2 == ""){
			
		} else {
			WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/company_textbox"), param2, FailureHandling.STOP_ON_FAILURE)
			WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/invoiceno_textbox"), param2b, FailureHandling.STOP_ON_FAILURE)
		}
	} else if (param1 == "pasienname"){
		if (param2 == ""){
			
		} else {
			WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/company_textbox"), param2, FailureHandling.STOP_ON_FAILURE)
			WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/pasienname_textbox"), param2b, FailureHandling.STOP_ON_FAILURE)
		}
	} else if (param1 == "date"){
		if (param2 == ""){
				
		} else {
			WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/company_textbox"), param2, FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject("GMA-UI/ClaimPayMon/startdate_icon"), FailureHandling.STOP_ON_FAILURE)
			GEN5.DatePicker(param2b, findTestObject("GMA-UI/ClaimPayMon/startdate_datepicker"))
			
			WebUI.click(findTestObject("GMA-UI/ClaimPayMon/enddate_icon"), FailureHandling.STOP_ON_FAILURE)
			//GEN5.DatePicker(param2c, findTestObject("GMA-UI/UpDocClaim/enddate_datepicker"))
			//GEN5.Write(findTestObject("GMA-UI/UpDocClaim/enddate_textbox"), param2c)
		}
	} else if (param1 == "claimno"){
		if (param2 == ""){
			
		} else {
			WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/company_textbox"), param2, FailureHandling.STOP_ON_FAILURE)
			WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/claimno_textbox"), param2b, FailureHandling.STOP_ON_FAILURE)
		}
	} else {
		
	}
} else if (param == "claimstatus"){
	if (param1 == "invoiceno"){
		if (param2 == ""){
			
		} else {
			WebUI.click(findTestObject("GMA-UI/ClaimPayMon/claimstatus_combobox"), FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('GMA-UI/ClaimPayMon/claimstatus_list_combobox', ['docType' : param2]))
			WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/invoiceno_textbox"), param2b, FailureHandling.STOP_ON_FAILURE)
		}
	} else if (param1 == "pasienname"){
		if (param2 == ""){
			
		} else {
			WebUI.click(findTestObject("GMA-UI/ClaimPayMon/claimstatus_combobox"), FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('GMA-UI/ClaimPayMon/claimstatus_list_combobox', ['docType' : param2]))
			WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/pasienname_textbox"), param2b, FailureHandling.STOP_ON_FAILURE)
		}
	} else if (param1 == "date"){
		if (param2 == ""){
				
		} else {
			WebUI.click(findTestObject("GMA-UI/ClaimPayMon/claimstatus_combobox"), FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('GMA-UI/ClaimPayMon/claimstatus_list_combobox', ['docType' : param2]))
			WebUI.click(findTestObject("GMA-UI/ClaimPayMon/startdate_icon"), FailureHandling.STOP_ON_FAILURE)
			GEN5.DatePicker(param2b, findTestObject("GMA-UI/ClaimPayMon/startdate_datepicker"))
			
			WebUI.click(findTestObject("GMA-UI/ClaimPayMon/enddate_icon"), FailureHandling.STOP_ON_FAILURE)
			//GEN5.DatePicker(param2c, findTestObject("GMA-UI/UpDocClaim/enddate_datepicker"))
			//GEN5.Write(findTestObject("GMA-UI/UpDocClaim/enddate_textbox"), param2c)
		}
	} else if (param1 == "claimno"){
		if (param2 == ""){
			
		} else {
			WebUI.click(findTestObject("GMA-UI/ClaimPayMon/claimstatus_combobox"), FailureHandling.STOP_ON_FAILURE)
			WebUI.click(findTestObject('GMA-UI/ClaimPayMon/claimstatus_list_combobox', ['docType' : param2]))
			WebUI.setText(findTestObject("GMA-UI/ClaimPayMon/claimno_textbox"), param2b, FailureHandling.STOP_ON_FAILURE)
		}
	} else {
		
	}
} else {
	
}

WebUI.click(findTestObject("GMA-UI/ClaimPayMon/search_button"), FailureHandling.STOP_ON_FAILURE)