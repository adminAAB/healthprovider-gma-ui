import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI

/*
if (WebUI.verifyElementPresent(findTestObject('GMA-UI/ClaimPayMon/close_popup'), 2, FailureHandling.OPTIONAL)) {
	WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/ClaimPayMon/ClosePopUpSearchAction'),
		[ ('') : '' ],
		FailureHandling.STOP_ON_FAILURE)
	println ('Download Gagal - Button tidak bisa di klik karena tidak ada data d grid')
} else{
	WebUI.click(findTestObject("GMA-UI/ClaimPayMon/download_button"), FailureHandling.STOP_ON_FAILURE)
}
*/

if (WebUI.click(findTestObject("GMA-UI/ClaimPayMon/download_button"), FailureHandling.OPTIONAL)) {
	WebUI.click(findTestObject("GMA-UI/ClaimPayMon/download_button"), FailureHandling.STOP_ON_FAILURE)
} else {
	println ('Download Gagal - Button tidak bisa di klik karena tidak ada data d grid')
}