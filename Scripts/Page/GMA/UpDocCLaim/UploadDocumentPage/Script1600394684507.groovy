import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5 as GEN5
import com.keyword.UI as UI
/*
String membername = "VANESSA CASSANDRA"
String picturename = "Biling Perbandingan.png"
String invoiceno = "AUTO-DAMI00001"

WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/UpDocCLaim/SearchPage'),
		[ ('param') : 'outstanding',
		('param2') : 'membername',
		('membername') : membername ],
		FailureHandling.STOP_ON_FAILURE)
*/

//GEN5.ClickExpectedRowWithNext(findTestObject("GMA-UI/UpDocClaim/outstanding_gridtable"), 
//	"Nama Peserta", membername, findTestObject("GMA-UI/UpDocClaim/nextgriddata_button"))

if (param == "outstanding"){
	GEN5.ClickExpectedRow(findTestObject("GMA-UI/UpDocClaim/outstanding_gridtable"), "Nama Peserta", membername)
} else if (param == "pending") {
	GEN5.ClickExpectedRow(findTestObject("GMA-UI/UpDocClaim/pending_gridtable"), "Nama Peserta", membername)
} else{
	println ("param tidak ada")
}


WebUI.click(findTestObject("GMA-UI/UpDocClaim/upload_button"), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject("GMA-UI/UpDocClaim/uploaddocument_button"), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

UI.UploadFile2(picturename)

WebUI.delay(2)
/*
WebUI.setText(findTestObject("GMA-UI/UpDocClaim/invoiceno_textbox"), invoiceno, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject("GMA-UI/UpDocClaim/submit_button"), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject("GMA-UI/UpDocClaim/submitconfirmation_popup"), FailureHandling.STOP_ON_FAILURE)
*/