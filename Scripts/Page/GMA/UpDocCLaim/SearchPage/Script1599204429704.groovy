import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil

import com.keyword.GEN5
import com.keyword.UI

/*
// Tidak bisa Call TestCase "UpDocClaimPage" Jadi Mengambil Isi Scriptnya - WithEdit
WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/Login/LoginPage'), [('password') : GlobalVariable.Password, ('username') : GlobalVariable.Username], FailureHandling.STOP_ON_FAILURE)
WebUI.waitForElementClickable(findTestObject('GMA-UI/UpDocClaim/UpDocClaim_menu'), GlobalVariable.maxDelay)
WebUI.click(findTestObject('GMA-UI/UpDocClaim/UpDocClaim_menu'))
WebUI.waitForElementClickable(findTestObject('GMA-UI/UpDocClaim/search_button'), GlobalVariable.maxDelay)
// End  "UpDocClaimPage"
*/


WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/UpDocCLaim/UpDocClaimPage'),
	[ ('') : ''],
	FailureHandling.STOP_ON_FAILURE)

if(param == "outstanding"){
	if (param2 == "date"){
		if (startdate == ""){
			
		} else {
			WebUI.click(findTestObject("GMA-UI/UpDocClaim/startdate_icon"), FailureHandling.STOP_ON_FAILURE)
			GEN5.DatePicker(startdate, findTestObject("GMA-UI/UpDocClaim/startdate_datepicker"))
			qGetTotalOutstanding = qGetTotalOutstanding + " AND CH.Start >= '" +  startdate + "'"
		}
		
		if (enddate == ""){
		
		// Fungsi GEN5.DatePicker tidak bisa, diganti fungsi DateNow dengan Clik Icon EndDate
		} else {
			WebUI.click(findTestObject("GMA-UI/UpDocClaim/enddate_icon"), FailureHandling.STOP_ON_FAILURE)
			findTestObject("GMA-UI/UpDocClaim/enddate_datepicker") //tmd
			//GEN5.Write(findTestObject("GMA-UI/UpDocClaim/enddate_textbox"), enddate)
			qGetTotalOutstanding = qGetTotalOutstanding + " AND CH.Finish <= '" +  enddate + "'"
		}
		
		WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
		
	} else if (param2 == "membername"){
		if (membername == ""){
			
		} else {
			GEN5.Write(findTestObject("GMA-UI/UpDocClaim/membername_textbox"), membername)
			WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
			qGetTotalOutstanding = qGetTotalOutstanding + " AND pm.Name = '" +  membername + "'"
		}
	} else if (param2 == "twoparam"){
		if (startdate == ""){
			
		} else {
			WebUI.click(findTestObject("GMA-UI/UpDocClaim/startdate_icon"), FailureHandling.STOP_ON_FAILURE)
			GEN5.DatePicker(startdate, findTestObject("GMA-UI/UpDocClaim/startdate_datepicker"))
			qGetTotalOutstanding = qGetTotalOutstanding + " AND CH.Start >= '" +  startdate + "'"
		}
		
		if (enddate == ""){
		
		// Fungsi GEN5.DatePicker tidak bisa, diganti fungsi DateNow dengan Clik Icon EndDate
		} else {
			WebUI.click(findTestObject("GMA-UI/UpDocClaim/enddate_icon"), FailureHandling.STOP_ON_FAILURE)
			findTestObject("GMA-UI/UpDocClaim/enddate_datepicker") //tmd
			//GEN5.Write(findTestObject("GMA-UI/UpDocClaim/enddate_textbox"), enddate)
			qGetTotalOutstanding = qGetTotalOutstanding + " AND CH.Finish <= '" +  enddate + "'"
		}
		
		GEN5.Write(findTestObject("GMA-UI/UpDocClaim/membername_textbox"), membername)
		WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
		qGetTotalOutstanding = qGetTotalOutstanding + " AND pm.Name = '" +  membername + "'"
		
		WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
		
	} else {
		WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
		
	}
	
	String[] getTotalData = UI.getOneColumnDatabase("172.16.94.70", "SEA", qGetTotalOutstanding.replace('_TreatmentPlace_', 'OJKRI00001'), "totalData")
	int random = new Random().nextInt(getTotalData.length)
	int iTotalDataDB = getTotalData[random].toInteger()
	iTotalDataDB = (Math.ceil(iTotalDataDB/10))
	println ('totalDB ' + iTotalDataDB)
	
	//CHECK KESESUAIAN TAMPILAN
	String strTotalUI = WebUI.getText(findTestObject("GMA-UI/UpDocClaim/btn-NavigationInfo"), FailureHandling.STOP_ON_FAILURE)
	strTotalUI = strTotalUI.replace('1 of ', '')
	println ('totalUI ' + strTotalUI)
	
	if (iTotalDataDB == 0) {
		iTotalDataDB = 1
	}
	
	if (iTotalDataDB.toString() != strTotalUI) {
		KeywordUtil.markFailedAndStop('Total Db dan UI tidak sama !, db : '+ iTotalDataDB.toString() + ' UI : '+strTotalUI)
	}
	
}else if(param == "pending"){
	WebUI.waitForElementClickable(findTestObject("GMA-UI/UpDocClaim/pending_tab"), GlobalVariable.maxDelay)
	WebUI.click(findTestObject("GMA-UI/UpDocClaim/pending_tab"), FailureHandling.STOP_ON_FAILURE)
	if (param2 == "date"){
		if (startdate == ""){
			
		} else {
			WebUI.click(findTestObject("GMA-UI/UpDocClaim/startdate_icon"), FailureHandling.STOP_ON_FAILURE)
			GEN5.DatePicker(startdate, findTestObject("GMA-UI/UpDocClaim/startdate_datepicker"))
			qGetTotalPending1 = qGetTotalPending1 + " AND ch.Start >= '" + startdate + "'"
		}
		
		if (enddate == ""){
		
		// Fungsi GEN5.DatePicker tidak bisa, diganti fungsi DateNow dengan Clik Icon EndDate
		} else {
			WebUI.click(findTestObject("GMA-UI/UpDocClaim/enddate_icon"), FailureHandling.STOP_ON_FAILURE)
			findTestObject("GMA-UI/UpDocClaim/enddate_datepicker") //tmd
			qGetTotalPending1 = qGetTotalPending1 + " AND ch.Finish <= '" + enddate + "'"
		}
		WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
		
	} else if (param2 == "membername"){
		if (membername == ""){
		
		} else {
			GEN5.Write(findTestObject("GMA-UI/UpDocClaim/membername_textbox"), membername)
			WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
			qGetTotalPending1 = qGetTotalPending1 + " AND pm.Name = '" + membername + "'"
		}
	} else if (param2 == "twoparam"){
		if (startdate == ""){
			
		} else {
			WebUI.click(findTestObject("GMA-UI/UpDocClaim/startdate_icon"), FailureHandling.STOP_ON_FAILURE)
			GEN5.DatePicker(startdate, findTestObject("GMA-UI/UpDocClaim/startdate_datepicker"))
			qGetTotalOutstanding = qGetTotalOutstanding + " AND CH.Start >= '" +  startdate + "'"
		}
		
		if (enddate == ""){
		
		// Fungsi GEN5.DatePicker tidak bisa, diganti fungsi DateNow dengan Clik Icon EndDate
		} else {
			WebUI.click(findTestObject("GMA-UI/UpDocClaim/enddate_icon"), FailureHandling.STOP_ON_FAILURE)
			findTestObject("GMA-UI/UpDocClaim/enddate_datepicker") //tmd
			//GEN5.Write(findTestObject("GMA-UI/UpDocClaim/enddate_textbox"), enddate)
			qGetTotalOutstanding = qGetTotalOutstanding + " AND CH.Finish <= '" +  enddate + "'"
		}
		
		GEN5.Write(findTestObject("GMA-UI/UpDocClaim/membername_textbox"), membername)
		WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
		qGetTotalOutstanding = qGetTotalOutstanding + " AND pm.Name = '" +  membername + "'"
		
		WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
		
	} else {
		WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
		
	}
	
	String qTotalPending = qGetTotalPending1 + " " + qGetTotalPending2
	String[] getTotalDataPending = UI.getOneColumnDatabase("172.16.94.70", "SEA", qTotalPending.replace('_TreatmentPlace_', 'OJKRI00001'), "totalData")
	int random = new Random().nextInt(getTotalDataPending.length)
	String strTotalData = getTotalDataPending[random].toString()
	println (strTotalData)
} else {
		WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
		
	}

