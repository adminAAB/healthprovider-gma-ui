import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI

/*
// Tidak bisa Call TestCase "UpDocClaimPage" Jadi Mengambil Isi Scriptnya - WithEdit
WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/Login/LoginPage'), [('password') : GlobalVariable.Password, ('username') : GlobalVariable.Username], FailureHandling.STOP_ON_FAILURE)
WebUI.waitForElementClickable(findTestObject('GMA-UI/UpDocClaim/UpDocClaim_menu'), GlobalVariable.maxDelay)
WebUI.click(findTestObject('GMA-UI/UpDocClaim/UpDocClaim_menu'))
WebUI.waitForElementClickable(findTestObject('GMA-UI/UpDocClaim/search_button'), GlobalVariable.maxDelay)
// End  "UpDocClaimPage"
*/

WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/UpDocCLaim/UpDocClaimPage'),
	[ ('') : ''],
	FailureHandling.STOP_ON_FAILURE)

if (param == "date"){
	WebUI.click(findTestObject("GMA-UI/UpDocClaim/startdate_icon"), FailureHandling.STOP_ON_FAILURE)
	GEN5.DatePicker(startdate, findTestObject("GMA-UI/UpDocClaim/startdate_datepicker"))
	WebUI.click(findTestObject("GMA-UI/UpDocClaim/enddate_icon"), FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
	
} else if (param == "membername"){
	GEN5.Write(findTestObject("GMA-UI/UpDocClaim/membername_textbox"), membername)
	WebUI.click(findTestObject("GMA-UI/UpDocClaim/search_button"), FailureHandling.STOP_ON_FAILURE)
	
} else {
	WebUI.click(findTestObject("GMA-UI/UpDocClaim/clear_button"), FailureHandling.STOP_ON_FAILURE)
	
}