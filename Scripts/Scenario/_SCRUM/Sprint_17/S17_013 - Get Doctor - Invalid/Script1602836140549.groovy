import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


import org.openqa.selenium.Keys as Keys
import com.keyword.GEN5
import com.keyword.UI
import com.keyword.API


WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/EligibilityDischarge/DischargePage'),
	[ ('') : '' ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/EligibilityDischarge/SearchMemberDischargeAction'),
	[ ('param') : 'success' ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/EligibilityDischarge/SearchDoctorAction'),
	[ ('doctor') : 'Invalid' ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.closeBrowser()
/*
WebUI.click(findTestObject("GMA-UI/ClaimPayMon/claimstatus_combobox"), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('GMA-UI/ClaimPayMon/claimstatus_list_combobox', ['docType' : param2]))

WebUI.delay(3)

WebUI.waitForElementClickable(findTestObject('Object Repository/GMA-UI/EligibilityDischarge/combobox_diagnosa'), GlobalVariable.maxDelay)

WebUI.click(findTestObject('Object Repository/GMA-UI/EligibilityDischarge/combobox_diagnosa'))

WebUI.click(findTestObject('Object Repository/GMA-UI/EligibilityDischarge/combobox_diagnosa'))

WebUI.sendKeys(findTestObject('Object Repository/GMA-UI/EligibilityDischarge/combobox_diagnosa')
	, Keys.chord(Keys.ARROW_DOWN))

WebUI.sendKeys(findTestObject('Object Repository/GMA-UI/EligibilityDischarge/combobox_diagnosa')
	, Keys.chord(Keys.ENTER))
*/





