import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


import com.keyword.GEN5
import com.keyword.UI
import com.keyword.API


WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/EligibilityDischarge/VoidPage'),
	[ ('') : '' ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/EligibilityDischarge/SearchMemberAction'),
	[ ('param') : 'success' ],
	FailureHandling.STOP_ON_FAILURE)

Date today = new Date()
String todaysDate = today.format('ddMMyyyy')
chars = '1234567890'
int length = 10
Random rand = new Random();
StringBuilder sb = new StringBuilder();
for (int i=0; i<length; i++) {
  sb.append(chars.charAt(rand.nextInt(chars.length())));
}

String Reason = 'KATALON-DWF' + todaysDate + '-' +sb.toString() 

UI.Note(Reason)

WebUI.setText(findTestObject('GMA-UI/EligibilityDischarge/textarea_reason')
	, Reason, FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/EligibilityDischarge/SubmitVoidAction'),
	[ ('Reason') : Reason ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('Object Repository/GMA-UI/EligibilityDischarge/button_close_popup'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.closeBrowser()




