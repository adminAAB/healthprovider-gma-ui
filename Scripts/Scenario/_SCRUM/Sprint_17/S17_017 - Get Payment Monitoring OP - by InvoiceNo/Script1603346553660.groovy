import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI
import com.kms.katalon.util.CryptoUtil


def oriPass = 'P@ssw0rd'
def encryptedPass = CryptoUtil.encode(CryptoUtil.getDefault(oriPass))
println(encryptedPass)

GlobalVariable.Username = 'jmeterGMA@asuransiastra.com'
GlobalVariable.Password  = encryptedPass

String[] GetInvoiceNo = UI.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GMA_QueryGetInvoiceData, "InvoiceNo")
int random = new Random().nextInt(GetInvoiceNo.length)
String InvoiceNo = GetInvoiceNo[random].toString()
println (InvoiceNo)

WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/ClaimPayMon/SearchPayMonPage'),
	[ ('param') : 'invoiceno',
	('param2') : 'HS-ION-101',
	('param3') : '' ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/ClaimPayMon/ClosePopUpSearchAction'),
[ ('') : '' ],
FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

