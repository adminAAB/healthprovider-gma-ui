import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil

import com.keyword.GEN5
import com.keyword.UI

String[] GetPending = UI.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GMA_QueryGetPendingData, "NamaPeserta")
int random = new Random().nextInt(GetPending.length)
String Pending = GetPending[random].toString()
println (Pending)

String picturename = "Materai 6K.jpg"

WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/UpDocCLaim/SearchPage'),
		[ ('param') : 'pending',
		 ('param2') : 'membername',
		('membername') : Pending ],
		FailureHandling.STOP_ON_FAILURE)

String[] CNO = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetCNO.replace('_MemberName_', Pending), "CNO")
println (CNO[0])

WebUI.delay(3)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/UpDocClaim/ClosePopUpSearchAction'),
	[ ('') : '' ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/UpDocClaim/GetTotalDocUpload'),
	[ ('ClaimNo') : CNO[0] ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/UpDocClaim/GetTotalDocVerify'),
	[ ('ClaimNo') : CNO[0] ],
	FailureHandling.STOP_ON_FAILURE)

//GlobalVariable.totalDocVerify = 0

if (GlobalVariable.totalDocUpload != GlobalVariable.totalDocVerify) {
		KeywordUtil.markFailedAndStop('Total GMA dan CoreHealth tidak sama !, GMA : '
			+ GlobalVariable.totalDocUpload.toString() + ' CoreHealth : '+GlobalVariable.totalDocVerify.toString())
	}

WebUI.closeBrowser()