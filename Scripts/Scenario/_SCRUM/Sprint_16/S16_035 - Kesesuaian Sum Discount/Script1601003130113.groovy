import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil

import com.keyword.GEN5
import com.keyword.UI
import com.kms.katalon.util.CryptoUtil

def oriPass = 'P@ssw0rd'
def encryptedPass = CryptoUtil.encode(CryptoUtil.getDefault(oriPass))
println(encryptedPass)

String ProviderID = 'TJKRM00017'
String DateCutOff = '2020-01-01'
GlobalVariable.Username = 'jmeterGMA@asuransiastra.com'
GlobalVariable.Password  = encryptedPass

String[] GetInvoiceNo = UI.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GMA_QueryGetInvoiceData, "InvoiceNo")
int random = new Random().nextInt(GetInvoiceNo.length)
//String InvoiceNo = GetInvoiceNo[random].toString()
String InvoiceNo = 'DWF-TESTDISCOUNT'
println (InvoiceNo)

WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/ClaimPayMon/SearchPayMonPage'),
	[ ('param') : 'invoiceno',
	('param2') : InvoiceNo,
	('param3') : '' ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/ClaimPayMon/ClosePopUpSearchAction'),
[ ('') : '' ],
FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/ClaimPayMon/GetSumDiscount'),
	[ ('ProviderID') : ProviderID ,
	('MemberName') : '' ,
	('InvoiceNoSearch') : InvoiceNo ,
	('PerusahaanSearch') : '' ,
	('DateFROMPembayaran') : '' ,
	('DateToPembayaran') : '' ,
	('StatusClaim') : '' ,
	('NomorClaimSearch') : '' ,
	('DateCutOff') : DateCutOff ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/ClaimPayMon/GetSumDiscountVerify'),
	[ ('ProviderID') : ProviderID ,
	('InvoiceNoSearch') : InvoiceNo ,
	('DateCutOff') : DateCutOff ],
	FailureHandling.STOP_ON_FAILURE)

if (GlobalVariable.SumTotalPayment != GlobalVariable.SumTotalPaymentVerify) {
	KeywordUtil.markFailedAndStop('Total SUM Total Receipt tidak sama !, API : '
		+ GlobalVariable.SumTotalPayment.toString() + ' Query : '+GlobalVariable.SumTotalPaymentVerify.toString())
}

if (GlobalVariable.SumTotalBilled != GlobalVariable.SumTotalBilledVerify) {
	KeywordUtil.markFailedAndStop('Total SUM Total Payment tidak sama !, API : '
		+ GlobalVariable.SumTotalBilled.toString() + ' Query : '+GlobalVariable.SumTotalBilledVerify.toString())
}

if (GlobalVariable.SumTotalDiscount != GlobalVariable.SumTotalPaymentVerify) {
	KeywordUtil.markFailedAndStop('Total SUM Total Discount tidak sama !, API : '
		+ GlobalVariable.SumTotalDiscount.toString() + ' Query : '+GlobalVariable.SumTotalPaymentVerify.toString())
}

WebUI.closeBrowser()

