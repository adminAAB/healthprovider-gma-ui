import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI

String[] GetOutstanding = UI.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GMA_QueyGetOutstandingData, "NamaPeserta")
int random = new Random().nextInt(GetOutstanding.length)
String Outstanding = GetOutstanding[random].toString()
println (Outstanding)

String picturename = "testPDF.pdf"

WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/UpDocCLaim/SearchPage'),
		[ ('param') : 'outstanding',
		('param2') : 'membername',
		('membername') : Outstanding ],
		FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/UpDocClaim/ClosePopUpSearchAction'),
	[ ('') : '' ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Page/GMA/UpDocCLaim/UploadDocumentPage'),
	[ ('param') : 'outstanding',
	('membername') : Outstanding,
	('picturename') : picturename ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()