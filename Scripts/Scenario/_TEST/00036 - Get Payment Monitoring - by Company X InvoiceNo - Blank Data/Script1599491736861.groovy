import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI
import com.keyword.REA

//String[] GetCompany = UI.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GMA_QueryGetInvoiceData, "Company")
//int random = new Random().nextInt(GetOneRowData.length)
//String OneRowData = GetOneRowData[random].toString()
//println (get(OneRowData,3))
//println (OneRowData.get(1))

ArrayList checkDB
String strCompany = ''
String strInvoiceNo = ''

checkDB = REA.getAllDataDatabase('172.16.94.70', 'SEA', GlobalVariable.GMA_QueryGetInvoiceData2)

for (i = 0; i < checkDB.size; i++) {
	strCompany = checkDB.get(i).get(3).trim()
	strInvoiceNo = checkDB.get(i).get(1).trim()
	println (strCompany)
	println (strInvoiceNo)
}

WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/ClaimPayMon/SearchPayMonPage'),
	[ ('param') : 'company',
	('param1') : 'invoiceno',
	('param2') : '',
	('param2b') : strInvoiceNo,
	('param2c') : '',
	('param3') : '' ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/ClaimPayMon/ClosePopUpSearchAction'),
[ ('') : '' ],
FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

