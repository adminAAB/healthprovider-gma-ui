import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI

String[] GetMemberName = UI.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GMA_QueryGetInvoiceData, "MemberName")
int random = new Random().nextInt(GetMemberName.length)
String MemberName = GetMemberName[random].toString()
println (MemberName)

String[] GetDateStart = UI.getOneColumnDatabase("172.16.94.70", "SEA", "SELECT REPLACE(CONVERT(VARCHAR(11),GETDATE()-30,106), ' ','/') AS StartDate", "StartDate")
random = new Random().nextInt(GetDateStart.length)
String strDateStart = GetDateStart[random].toString()
println (strDateStart)


String[] GetDateEnd = UI.getOneColumnDatabase("172.16.94.70", "SEA", "SELECT REPLACE(CONVERT(VARCHAR(11),GETDATE(),106), ' ','/') AS StartDate", "StartDate")
random = new Random().nextInt(GetDateEnd.length)
String strDateEnd = GetDateEnd[random].toString()
println (strDateEnd)

WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/ClaimPayMon/SearchPayMonPage'),
	[ ('param') : 'date',
	('param2') : strDateStart, //'01/Jan/2020',
	('param3') : '' ],
	FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.callTestCase(findTestCase('Test Cases/Scenario/GMA/ClaimPayMon/ClosePopUpSearchAction'),
[ ('') : '' ],
FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

