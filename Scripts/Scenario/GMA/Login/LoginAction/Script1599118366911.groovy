import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI

import com.kms.katalon.util.CryptoUtil

def decryptedPass = (CryptoUtil.decode(CryptoUtil.getDefault(password)))
//println(decryptedPass)

WebUI.callTestCase(findTestCase('API/Login/SignInAPI'),
	[('password') : decryptedPass,
	('username') : username],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Test Cases/Page/GMA/Login/LoginPage'), 
	[('password') : decryptedPass, 
	('username') : username], 
	FailureHandling.STOP_ON_FAILURE)

//GEN5.ProcessingCommand()
//WebUI.delay(5)

if (WebUI.verifyElementPresent(findTestObject('GMA-UI/Login/close_popup'), 1, FailureHandling.OPTIONAL)) {
	WebUI.click(findTestObject("GMA-UI/Login/close_popup"), FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('GMA-UI/Login/masuk_button'))
} else{
	println ('Login Berhasil')
}

if (WebUI.verifyElementPresent(findTestObject('GMA-UI/Login/close_popup'), 1, FailureHandling.OPTIONAL)) {
	WebUI.click(findTestObject("GMA-UI/Login/close_popup"), FailureHandling.STOP_ON_FAILURE)
	WebUI.click(findTestObject('GMA-UI/Login/masuk_button'))
} else{
	println ('Login Berhasil')
}





