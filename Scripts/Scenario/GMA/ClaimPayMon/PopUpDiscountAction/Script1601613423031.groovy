import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

if (param == "yes"){
	WebUI.verifyElementPresent(findTestObject('GMA-UI/ClaimPayMon/yes_popupdiscount_button'), 2, FailureHandling.OPTIONAL)
	WebUI.click(findTestObject("GMA-UI/ClaimPayMon/yes_popupdiscount_button"), FailureHandling.STOP_ON_FAILURE)
} else if (param == "no") {
	WebUI.verifyElementPresent(findTestObject('GMA-UI/ClaimPayMon/yes_popupdiscount_button'), 2, FailureHandling.OPTIONAL)
	WebUI.click(findTestObject("GMA-UI/ClaimPayMon/yes_popupdiscount_button"), FailureHandling.STOP_ON_FAILURE)
} else {
	println ('Search Berhasil')
}


WebUI.delay(3)