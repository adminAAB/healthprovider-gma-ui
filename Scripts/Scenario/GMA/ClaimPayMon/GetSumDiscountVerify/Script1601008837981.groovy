import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI

//String ClaimNo = '2834817'


println (ProviderID)
println (InvoiceNoSearch)
println (DateCutOff)

println (GetSumDiscountVerify)
GetSumDiscountVerify = GetSumDiscountVerify.replace( '_ProviderID_',ProviderID )
GetSumDiscountVerify = GetSumDiscountVerify.replace( '_InvoiceNoSearch_',InvoiceNoSearch )
GetSumDiscountVerify = GetSumDiscountVerify.replace( '_DateCutOff_',DateCutOff )
println (GetSumDiscountVerify)

String[] SumTotalPaymentVerify = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetSumDiscountVerify, "SumTotalPayment")
GlobalVariable.SumTotalPaymentVerify = SumTotalPaymentVerify.length
println (GlobalVariable.SumTotalPayment)

String[] SumTotalBilledVerify = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetSumDiscountVerify, "SumTotalBilled")
GlobalVariable.SumTotalBilledVerify = SumTotalBilledVerify.length
println (GlobalVariable.SumTotalBilled)

String[] SumTotalDiscountVerify = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetSumDiscountVerify, "SumTotalDiscount")
GlobalVariable.SumTotalDiscountVerify = SumTotalDiscountVerify.length
println (GlobalVariable.SumTotalDiscount)
