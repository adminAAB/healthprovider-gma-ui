import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI

//String ClaimNo = '2834817'

println (ProviderID)
println (InvoiceNoSearch)
println (DateCutOff)

println (GetSumDiscount)
GetSumDiscount = GetSumDiscount.replace( '_ProviderID_',ProviderID )
GetSumDiscount = GetSumDiscount.replace( '_MemberName_',MemberName )
GetSumDiscount = GetSumDiscount.replace( '_InvoiceNoSearch_',InvoiceNoSearch )
GetSumDiscount = GetSumDiscount.replace( '_PerusahaanSearch_',PerusahaanSearch )
GetSumDiscount = GetSumDiscount.replace( '_DateFROMPembayaran_',DateFROMPembayaran )
GetSumDiscount = GetSumDiscount.replace( '_DateToPembayaran_',DateToPembayaran )
GetSumDiscount = GetSumDiscount.replace( '_StatusClaim_',StatusClaim )
GetSumDiscount = GetSumDiscount.replace( '_NomorClaimSearch_',NomorClaimSearch )
GetSumDiscount = GetSumDiscount.replace( '_DateCutOff_',DateCutOff )
println (GetSumDiscount)

String[] SumTotalPayment = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetSumDiscount, "SumTotalPayment")
GlobalVariable.SumTotalPayment = SumTotalPayment.length
println (GlobalVariable.SumTotalPayment)

String[] SumTotalBilled = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetSumDiscount, "SumTotalBilled")
GlobalVariable.SumTotalBilled = SumTotalBilled.length
println (GlobalVariable.SumTotalBilled)

String[] SumTotalDiscount = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetSumDiscount, "SumTotalDiscount")
GlobalVariable.SumTotalDiscount = SumTotalDiscount.length
println (GlobalVariable.SumTotalDiscount)
