import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI

//String ClaimNo = '2834817'

String[] _DocumentNo_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "DocumentNo")
String[] _AdditionalInfo_ = 0
String[] _ClaimType_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "ClaimType")
String[] _Billed_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "Billed")
String[] _ProductType_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "ProductType")
String[] _RoomOption_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "RoomOption") 
String[] _isCobBPJS_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "isCobBPJS")
String[] _isCashPlanBPJS_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "isCashPlanBpjs")
String[] _isDoubleInsured_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "isDoubleInsured")
String[] _isNotEligible_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "isNoteEligible")
String[] _isSpecialTreatment_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "isSpecialTreatment")
String[] _isDeceased_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "isDeceased")
String[] _clientID_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "ClientID")
String[] _providerID_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "ProviderID")
String[] _diagnosisID_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "DiagnosisID")
String[] _treatmentperiod_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "TreatmentPeriod")
String[] _mno_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "MNO")
String[] _isTrafficAccident_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "isTrafficAccident")
String[] _ClaimIDMedcare_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "ClaimIDMedcare")
String[] _ClaimNo_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "CHClaimNumber")
String[] _IsInvoiceDisable_ = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetParamFromClaimNo.replace('_ClaimNoInput_', ClaimNo), "isInvoiceDisable")

println (GetClaimDocuments)
GetClaimDocuments = GetClaimDocuments.replace( '_DocumentNo_',_DocumentNo_[0].toString() )
GetClaimDocuments = GetClaimDocuments.replace('_AdditionalInfo_',_AdditionalInfo_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_ClaimType_',_ClaimType_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_Billed_',_Billed_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_ProductType_',_ProductType_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_RoomOption_',_RoomOption_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_isCobBPJS_',_isCobBPJS_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_isCashPlanBPJS_',_isCashPlanBPJS_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_isDoubleInsured_',_isDoubleInsured_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_isNotEligible_',_isNotEligible_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_isSpecialTreatment_',_isSpecialTreatment_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_isDeceased_',_isDeceased_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_clientID_',_clientID_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_providerID_',_providerID_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_diagnosisID_',_diagnosisID_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_treatmentperiod_',_treatmentperiod_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_mno_',_mno_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_isTrafficAccident_',_isTrafficAccident_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_ClaimIDMedcare_',_ClaimIDMedcare_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_ClaimNo_',_ClaimNo_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_IsInvoiceDisable_',_IsInvoiceDisable_[0].toString())
GetClaimDocuments = GetClaimDocuments.replace('_DocumentNo_',_DocumentNo_[0].toString())
println (GetClaimDocuments)

String[] getTotalData = UI.getOneColumnDatabase("172.16.94.70", "SEA", GetClaimDocuments, "DocumentTypeName")
GlobalVariable.totalDocUpload = getTotalData.length
println (GlobalVariable.totalDocUpload)

