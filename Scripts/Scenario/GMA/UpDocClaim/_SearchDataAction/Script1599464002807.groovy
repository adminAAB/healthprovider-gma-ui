import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.time.TimeCategory

import com.keyword.GEN5
import com.keyword.UI

//GlobalVariable.SearchScenarioData = 'pending_param_membername'
//SearchParam = 'Rahmat'

//Example Search Outstanding Data by Date
if (GlobalVariable.SearchScenarioData == 'outstanding_date'){
	WebUI.callTestCase(findTestCase('Page/GMA/UpDocCLaim/_SearchOutstandingPage'),
		[ ('param') : 'date',
		('startdate') : '31/Aug/2020',
		('enddate') : '3/Sep/2020' ],
		FailureHandling.STOP_ON_FAILURE)
	
//Example Search Outstanding Data By MemberName
} else if (GlobalVariable.SearchScenarioData == 'outstanding_membername') {
	String[] GetOutstanding = UI.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GMA_QueyGetOutstandingData, "NamaPeserta")
	int random = new Random().nextInt(GetOutstanding.length)
	String Outstanding = GetOutstanding[random].toString()
	println (Outstanding)
	
	WebUI.callTestCase(findTestCase('Page/GMA/UpDocCLaim/_SearchOutstandingPage'),
		[ ('param') : 'membername',
		('membername') : Outstanding ],
		FailureHandling.STOP_ON_FAILURE)
	
//Search Outstanding Data By Parameter Member Name
} else if (GlobalVariable.SearchScenarioData == 'outstanding_param_membername') {
	WebUI.callTestCase(findTestCase('Page/GMA/UpDocCLaim/_SearchOutstandingPage'),
		[ ('param') : 'membername',
		('membername') : SearchParam ],
		FailureHandling.STOP_ON_FAILURE)

//Search Pending Data by Parameter Member Name
} else if (GlobalVariable.SearchScenarioData == 'pending_param_membername') {
	WebUI.callTestCase(findTestCase('Page/GMA/UpDocCLaim/_SearchPendingPage'),
		[ ('param') : 'membername',
		('membername') : SearchParam ],
		FailureHandling.STOP_ON_FAILURE)
	
//Search Outstanding Data by Parameter Date
}else if (GlobalVariable.SearchScenarioData == 'outstanding_param_date') {
	WebUI.callTestCase(findTestCase('Page/GMA/UpDocCLaim/_SearchOutstandingPage'),
		[ ('param') : SearchParam,
		('membername') : SearchParam2 ],
		FailureHandling.STOP_ON_FAILURE)

//Search Pending Data by Parameter Member Date
} else if (GlobalVariable.SearchScenarioData == 'pending_param_membername') {
	WebUI.callTestCase(findTestCase('Page/GMA/UpDocCLaim/_SearchPendingPage'),
		[ ('param') : SearchParam,
		('membername') : SearchParam2 ],
		FailureHandling.STOP_ON_FAILURE)
	
}


 

