import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import org.openqa.selenium.Keys as Keys
import com.keyword.GEN5
import com.keyword.UI
import com.keyword.API

if (param == 'success') {
	String[] GetMember = UI.getOneColumnDatabase("172.16.94.70", "SEA", queryGetMember, "MemberNo")
	int random = new Random().nextInt(GetMember.length)
	String strMember = GetMember[random].toString()
	Member = strMember
} else if (param == 'failed') {
	String[] GetMember = UI.getOneColumnDatabase("172.16.94.70", "SEA", queryGetMember2, "MemberNo")
	int random = new Random().nextInt(GetMember.length)
	String strMember = GetMember[random].toString()
	Member = strMember
} else {
	Member = ''
}

GlobalVariable.temp1 = Member

UI.Note(GlobalVariable.temp1)

WebUI.callTestCase(findTestCase('Test Cases/API/EligibilityDischarge/GetEligibleMemberAPI'),
	[('member') : Member],
	FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('GMA-UI/EligibilityDischarge/sugestionbox_member'), Member)

WebUI.sendKeys(findTestObject('GMA-UI/EligibilityDischarge/sugestionbox_member')
	, Keys.chord(Keys.ENTER))
