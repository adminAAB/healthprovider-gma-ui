import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.API
import com.keyword.UI

//String strUsername = 'REI'
//String strPassword = 'ITG@nt1P455QC'

//Pengecekan Login

def var = WS.sendRequest(findTestObject('Object Repository/GMA-API/EligibilityDischarge/GetRegisteredEligibleMember',
	[('member'): member]))

if (var.getStatusCode() == 200 ) {
	API.Note(var.getResponseBodyContent())
	
	if (API.getResponseData(var).Status == true) {
		API.Note('API SUCCESS')
		
		API.Note(member)

		API.Note(API.getResponseData(var).Data[0])
		
		if (API.getResponseData(var).Data[0] == null){
			API.Note('Data Kosong')
		} else {
			if (member == API.getResponseData(var).Data[0].MemberNo){
				API.Note('Data Sesuai')
			} else {
				API.Note('Data Tidak Sesuai')
			}
		}
		
		/*
		if (member == API.getResponseData(var).Data[0].MemberNo){
			API.Note('Data Sesuai')
		} else {
			API.Note('Data Tidak Sesuai')
		}
		*/
		
	} else {
		API.Note('API FAILED')
	}
} else  {
	API.Note('Error Server - ' + var.getStatusCode())
}

