import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

import com.keyword.API
import com.keyword.UI

//String strUsername = 'REI'
//String strPassword = 'ITG@nt1P455QC'

//Pengecekan Login

def var = WS.sendRequest(findTestObject('GMA-API/Login/SignIn',
	[('username'): username,
	('password'): password]))

if (var.getStatusCode() == 200 ) {
	API.Note(var.getResponseBodyContent())
	
	if (API.getResponseData(var).status == true) {
		API.Note('Success Login')
				
		API.Note(API.getResponseData(var).token)
		GlobalVariable.Token =  API.getResponseData(var).token
	} else {
		API.Note('Login Failed')
	}
} else  {
	def var2 = WS.sendRequest(findTestObject('GMA-API/Login/SignIn',
		[('username'): username,
		('password'): password]))
	
	if (var2.getStatusCode() == 200 ) {
		API.Note(var2.getResponseBodyContent())
		
		if (API.getResponseData(var2).status == true) {
			API.Note('Success Login')
			
			API.Note(API.getResponseData(var2).token)
			GlobalVariable.Token =  API.getResponseData(var2).token
		} else {
			API.Note('Login Failed')
		}
	} else  {
		def var3 = WS.sendRequest(findTestObject('GMA-API/Login/SignIn',
			[('username'): username,
			('password'): password]))
		
		if (var3.getStatusCode() == 200 ) {
			API.Note(var3.getResponseBodyContent())
			
			if (API.getResponseData(var3).status == true) {
				API.Note('Success Login')
				
				API.Note(API.getResponseData(var3).token)
				GlobalVariable.Token =  API.getResponseData(var3).token
			} else {
				API.Note('Login Failed')
			}
		} else  {
			API.Note('Error Server - ' + var3.getStatusCode())
		}
	}
}

